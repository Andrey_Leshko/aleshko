package com.example.aleshko_l6_p1

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.aleshko_l6_p1.databinding.ActivityMainBinding


class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private val edit = "edit"
    private val save = "save"
    val PHOTO_CAMERA = 1
    private val CALL = 2
    private var landPhoto: Bitmap? = null
    private val PHOTO_KEY = "keyPhoto"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        if (savedInstanceState != null) {
            landPhoto = savedInstanceState.getParcelable(PHOTO_KEY)

        }
        if (landPhoto != null) {
            binding.addPhoto.setImageBitmap(landPhoto)
        }
        camera()
        callfun()
        mailfun()
        showEdit()
        binding.switchButton.setOnClickListener {
            when (binding.switchButton.text) {
                edit -> showSave()
                save -> showEdit()
            }
        }
    }

    fun showEdit() {
        binding.switchButton.text = edit
        binding.editName.isFocusable = false
        binding.editPhone.isFocusable = false
        binding.editMail.isFocusable = false
        binding.emailButton.visibility = View.VISIBLE
        binding.callButton.visibility = View.VISIBLE
        val imm: InputMethodManager =
            getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        if (imm.isActive)
            imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0)
    }

    fun showSave() {
        binding.switchButton.text = save
        binding.editName.isFocusableInTouchMode = true
        binding.editPhone.isFocusableInTouchMode = true
        binding.editMail.isFocusableInTouchMode = true
        binding.emailButton.visibility = View.GONE
        binding.callButton.visibility = View.GONE
    }

    fun camera() {
        binding.addPhoto.setOnClickListener {
            val permission =
                ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
            if (permission == PackageManager.PERMISSION_GRANTED) {
                val takePhoto = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                try {
                    startActivityForResult(takePhoto, PHOTO_CAMERA)
                } catch (e: Exception) {
                    Toast.makeText(this, "No camera", Toast.LENGTH_SHORT).show()
                }
            } else {
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    PHOTO_CAMERA
                )
            }
        }
    }

    fun callfun() {
        binding.callButton.setOnClickListener {
            val permission = ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE)
            if (permission == PackageManager.PERMISSION_GRANTED) {
                val intent = Intent(
                    Intent.ACTION_DIAL,
                    Uri.parse("tel:" + binding.editPhone.text.toString())
                )
                if (binding.editPhone.text.toString().isEmpty()) {
                    Toast.makeText(this, "the call field isEmpty", Toast.LENGTH_SHORT).show()
                } else {
                    startActivity(intent)
                }
            } else {
                ActivityCompat.requestPermissions(
                    this as Activity,
                    arrayOf(Manifest.permission.CALL_PHONE),
                    CALL
                )
            }

        }

    }

    fun mailfun() {
        binding.emailButton.setOnClickListener {
            if (binding.editMail.text.toString().isEmpty()) {
                Toast.makeText(this, "field Email isEmpty", Toast.LENGTH_SHORT).show()
            } else {
                val emailIntent = Intent(android.content.Intent.ACTION_SEND);
                emailIntent.setType("plain/text")
                emailIntent.putExtra(
                    android.content.Intent.EXTRA_EMAIL,
                    arrayOf(binding.editMail.text.toString())
                )
                startActivity(
                    Intent.createChooser(
                        emailIntent,
                        "Отправка письма..."
                    )
                )
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PHOTO_CAMERA && resultCode == RESULT_OK) {
            landPhoto = data?.extras?.get("data") as Bitmap
            binding.addPhoto.setImageBitmap(landPhoto)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelable(PHOTO_KEY, landPhoto)
    }

}


